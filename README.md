# Continuous Integration 3

Dans cet exercice, vous allez tenter de créer un runner pour projet swift.
Bien sûr ce genre d'activité se fait sur son ordinateur personnel en général (car c'est bien plus rapide et pratique). La visé de cet exercice est de vous apprendre à consulter le pipeline de CI/CD pourvu par GitLab. 

Un paquet swift a généralement ce genre d'arborescence:

.  
├── Package.swift  
├── README.md  
├── Sources  
│   └── temp  
│       └── main.swift  
└── Tests  
    └── tempTests  
        └── tempTests.swift  
  
4 directories, 4 files  

Il vous faut prendre un runner qui utilise un conteneur "Swift".

## Prérequis
- (pas obligatoire) Éditeur de swift (ou bloque note)
- L'outil git
- Un compte GitLab

## Mise en place

### Fork
Il faut tout d'abord "forker" le projet. Cela veut dire que vous allez avoir une copie de ce projet dans votre propre compte.

### Clone
Il vous suffit maintenant de cloner votre copie du projet sur votre ordinateur. Il suffit d'utiliser git clone.

Vous êtes prêt pour commencer.

### Exercice
Dans cet exercice, vous allez tenter de créer un runner pour lancer un test avec le fichier `tempTests.swift` et exécuter le fichier `main.swift`. Il n'y a pas un s'inquiéter car aucune connaissance de l'exécution d'un programme en Swift ne sera demandé. On se concentre plutôt sur le moyen de faire le test et l'exécution sur le CI. Il faudra juste vous renseigner sur comment faire des tests sur un projet Swift et exécuter son code. 

## Rendu
Vous devez présenter deux screenshots du terminal du CI:

- Le message de fin quand le processus s'est lancé sans erreur

